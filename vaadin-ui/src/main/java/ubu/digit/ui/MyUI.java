package ubu.digit.ui;

import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;

import org.apache.log4j.Logger;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import ubu.digit.pesistence.SistInfData;

/**
 *
 */
@SuppressWarnings("serial")
@Theme("mytheme")
@Widgetset("ubu.digit.ui.MyAppWidgetset")
public class MyUI extends UI {

	private static final Logger LOGGER = Logger.getLogger(MyUI.class);

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		final VerticalLayout layout = new VerticalLayout();
		SistInfData fachData = SistInfData.getInstance();
		layout.setMargin(true);
		setContent(layout);

        layout.addComponent(new Label("<h2>Tribunal</h2>", ContentMode.HTML));
		try {
			ResultSet result = fachData.getResultSet("Tribunal", "NombreApellidos");
			while (result.next()) {
				String cargo = stringToUTF8(result.getString("Cargo"));
				String nombre = stringToUTF8(result.getString("NombreApellidos"));
				layout.addComponent(new Label(cargo + ": " + nombre));
				LOGGER.info(cargo + ": " + nombre);
			}
			result.close();
		} catch (SQLException e) {
			LOGGER.error(e);
		}

        layout.addComponent(new Label("<h2>Especificaciones de Entrega</h2>", ContentMode.HTML));
		try {
			ResultSet result = fachData.getResultSet("Norma", "Descripcion");
			while (result.next()) {
				String descripcion = stringToUTF8(result.getString("Descripcion"));
				layout.addComponent(new Label(descripcion));
				LOGGER.info(descripcion);
			}
			result.close();
		} catch (SQLException e) {
			LOGGER.error(e);

		}

        layout.addComponent(new Label("<h2>Fechas de Entrega</h2>", ContentMode.HTML));
		BrowserFrame cal = new BrowserFrame("Calendario", new ExternalResource("http://goo.gl/PgEkF1"));
		cal.setSizeFull();
		cal.setHeight("500px");
		layout.addComponent(cal);

        layout.addComponent(new Label("<h2>Documentos</h2>", ContentMode.HTML));
		try {
			ResultSet result = fachData.getResultSet("Documento", "Descripcion");
			while (result.next()) {
				String descripcion = stringToUTF8(result.getString("Descripcion"));
				String url = stringToUTF8(result.getString("Url"));
				layout.addComponent(new Link(descripcion, new ExternalResource(url)));
				LOGGER.info(descripcion + ": " + url);
			}
			result.close();
		} catch (SQLException e) {
			LOGGER.error(e);
		}
	}

	private String stringToUTF8(String string) {
		return new String(string.getBytes(), StandardCharsets.UTF_8);
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {
	}
}
