package com.example.formulariovaadin;

import java.util.Date;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("formulariovaadin")
public class FormulariovaadinUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = FormulariovaadinUI.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		FormLayout layout = new FormLayout();
		layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		setContent(layout);

		Label label = new Label("Texto no editable");
		layout.addComponent(label);

		TextField textField = new TextField("Nombre");
		textField.setValue("Tu nombre");
		layout.addComponent(textField);

		DateField date = new DateField();
		date.setValue(new Date());
		date.setResolution(Resolution.DAY);
		layout.addComponent(date);

		CheckBox checkbox = new CheckBox("Checkbox");
		checkbox.setValue(true);
		layout.addComponent(checkbox);

		ListSelect select = new ListSelect("Lista");
		select.addItems("item1", "item2", "item3");
		select.setNullSelectionAllowed(false);
		select.setRows(3);
		select.setValue("item1");
		layout.addComponent(select);

		Button button = new Button("Actualizar");
		button.addClickListener(event -> label.setValue("Hola: " + textField.getValue() + ". Has seleccionado el dia: "
				+ date.getValue().toString() + ",el valor del checkbox es " + checkbox.getValue().toString()
				+ " y el item seleccionado es: " + select.getValue().toString()));
		layout.addComponent(button);
	}
}