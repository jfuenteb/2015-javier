package ubu.digit.ui;

import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import org.apache.log4j.Logger;

import ubu.digit.pesistence.SistInfData;

/**
 *
 */
@Theme("sistinftheme")
@Widgetset("ubu.digit.ui.SistInfWidgetset")
public class SistInfUI extends UI {
	private static final Logger LOGGER = Logger.getLogger(SistInfUI.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = -4568743602891945769L;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		SistInfData fachData = SistInfData.getInstance();
		VerticalLayout layout = new VerticalLayout();
		layout.setMargin(true);
		layout.setSpacing(true);
		layout.setWidth("80%");
		
		layout.addComponent(new Label("Cambios2!"));
		Label tribunal = new Label("Tribunal");
		tribunal.setWidthUndefined();
		tribunal.setStyleName("lbl-subtitulo");
		layout.addComponent(tribunal);
        
		try {
			ResultSet result = fachData.getResultSet("Tribunal", "NombreApellidos");
			while (result.next()) {
				String cargo = stringToUTF8(result.getString("Cargo"));
				String nombre = stringToUTF8(result.getString("NombreApellidos"));
				layout.addComponent(new Label(cargo + ": " + nombre));
			}
			result.close();
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		layout.addComponent(new Label("Programa en vigor a partir del Curso 2002-2003."));
		layout.addComponent(new Label("&nbsp;", ContentMode.HTML));
		
		Label especificaciones = new Label("Especificaciones de Entrega");
		especificaciones.setStyleName("lbl-subtitulo");
        layout.addComponent(especificaciones);
		try {
			ResultSet result = fachData.getResultSet("Norma", "Descripcion");
			while (result.next()) {
				String descripcion = stringToUTF8(result.getString("Descripcion"));
				layout.addComponent(new Label(descripcion));
			}
			result.close();
		} catch (SQLException e) {
			LOGGER.error(e);

		}
		layout.addComponent(new Label("&nbsp;", ContentMode.HTML));

		
        Label fechas = new Label("Fechas de Entrega");
        fechas.setStyleName("lbl-subtitulo");
		layout.addComponent(fechas);
		
		BrowserFrame cal = new BrowserFrame("Calendario", new ExternalResource("http://goo.gl/PgEkF1"));
		cal.setWidth(100, Unit.PERCENTAGE);
		cal.setHeight("500px");
		layout.addComponent(cal);
		layout.addComponent(new Label("&nbsp;", ContentMode.HTML));

		
        Label documentos = new Label("Documentos");
        documentos.setStyleName("lbl-subtitulo");
		layout.addComponent(documentos);
		try {
			ResultSet result = fachData.getResultSet("Documento", "Descripcion");
			while (result.next()) {
				String descripcion = stringToUTF8(result.getString("Descripcion"));
				String url = stringToUTF8(result.getString("Url"));
				layout.addComponent(new Link(descripcion, new ExternalResource(url)));
			}
			result.close();
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		
	

	}
	
	private String stringToUTF8(String string) {
		return new String(string.getBytes(), StandardCharsets.UTF_8);
	}

	@WebServlet(urlPatterns = "/*", name = "SistInfUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = SistInfUI.class, productionMode = false)
	public static class SistInfUIServlet extends VaadinServlet {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8278292941976902830L;
	}
}
